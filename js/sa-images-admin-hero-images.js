(function( $ ) {
    'use strict';

    // When the document is ready...
    $(document).ready(function() {

        // Set hero image <select>
        var $hero_image_select = $( '#sa-hero-image-select' );

        // Get hero images
        $.ajax( {
            url: 'https://sa.ua.edu/wp-json/wp/v2/hero_images?filter[posts_per_page]=-1',
            success: function ( $hero_images ) {

                // Make sure hero images are defined
                if ( $hero_images === undefined || $hero_images === null ) {
                    return false;
                }

                // Load select with hero images
                $.each( $hero_images, function( $index, $item ) {
                    $hero_image_select.append( '<option value="' + $item.id + '">' + $item.title.rendered + '</option>' );
                });

                // Define selected
                if ( $hero_image_select.data( 'selected' ) > 0 ) {
                    $hero_image_select.val( $hero_image_select.data( 'selected' ) );
                }

                // Make it a chosen element
                $hero_image_select.chosen();

            },
            cache: false
        } );

    });

})( jQuery );