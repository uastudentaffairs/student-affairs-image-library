<?php

// Setup admin styles and scripts
add_action( 'admin_enqueue_scripts', function ( $hook_suffix ) {

	// Get the plugin path
	$plugin_dir = plugin_dir_url( dirname( __FILE__ ) );

	// Load depending on the page
	switch( $hook_suffix ) {

		case 'post.php':
		case 'post-new.php':

			if ( current_user_can( 'select_hero_image' ) ) {

				// Register chosen style and script
				wp_enqueue_style( 'chosen', $plugin_dir . 'includes/chosen/chosen.min.css' );
				wp_register_script( 'chosen', $plugin_dir . 'includes/chosen/chosen.jquery.min.js', array( 'jquery' ), false, true );

				// Register our script
				wp_enqueue_script( 'sa-images-admin-hero-images', $plugin_dir . 'js/sa-images-admin-hero-images.min.js', array( 'jquery', 'chosen' ), false, true );

			}

			break;

	}

});

// Add meta boxes
add_action( 'add_meta_boxes', function ( $post_type, $post ) {

	// Add hero image meta box - if user has the capability
	if ( current_user_can( 'select_hero_image' ) ) {

		$hero_image_post_types = array( 'page' );
		if ( in_array( $post_type, $hero_image_post_types ) ) {
			add_meta_box( 'sa-hero-image', __( 'Hero Image', 'uastudentaffairs' ), 'print_sa_images_meta_boxes', $post_type, 'normal', 'core' );
		}

	}

}, 1, 2 );

// Print meta boxes
function print_sa_images_meta_boxes( $post, $metabox ) {

	switch( $metabox[ 'id' ] ) {

		case 'sa-hero-image':

			// Get selected ID
			$selected_sa_hero_image = get_post_meta( $post->ID, '_sa_hero_image', true );

			// Is loaded via JS
			?><p>Hero images are loaded via the <a href="https://sa.ua.edu/wp-admin/edit.php?post_type=hero_images" target="_blank">main Student Affairs website admin</a>. You must have permissions to access the admin and to load hero images. Once you've added the hero image to the admin, return to this page and find it in the dropdown list below.</p>
			
			<p>"Inside" page hero images are managed differently from your home page hero image. To change the hero image for your home page, visit the <a href="<?php echo admin_url( 'admin.php?page=student-affairs-child' ); ?>" target="_blank">home page settings</a>.</p>
			
			<p>The dimensions for your "inside" page hero images are 2000x300. The dimensions for the home page hero image is 2000x600.</p>
			<select id="sa-hero-image-select" name="sa_hero_image" style="width:100%;" data-selected="<?php echo $selected_sa_hero_image; ?>">
				<option value="">No hero image</option>
			</select><?php
			break;

	}

}

// Save posts
add_action( 'save_post', function( $post_id, $post, $update ) {

	// Pointless if $_POST is empty (this happens on bulk edit)
	if ( empty( $_POST ) ) {
		return;
	}

	// Disregard autosave
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	switch( $post->post_type ) {

		case 'page':

			// Verify that we're updating a post and user has capability
			if ( current_user_can( 'select_hero_image' ) && wp_verify_nonce( $_POST[ '_wpnonce' ], 'update-post_' . $_POST[ 'post_ID' ] ) ) {

				// Make sure field is set
				if ( isset( $_POST[ 'sa_hero_image' ] ) ) {

					// Sanitize user input.
					$sa_hero_image_value = sanitize_text_field( $_POST[ 'sa_hero_image' ] );

					// Update/save value
					update_post_meta( $post_id, '_sa_hero_image', $sa_hero_image_value );

				}

			}

			break;

	}

}, 10, 3 );

// Add options page and field groups
add_action( 'admin_menu', function() {

	if ( function_exists( 'acf_add_local_field_group' ) ):

		// Add home page field group
		acf_add_local_field_group(array (
			'key' => 'group_569907a348704',
			'title' => 'Home Page',
			'fields' => array (
				array (
					'key' => 'field_569907a823fda',
					'label' => 'Hero Image',
					'name' => 'front_page_hero_image',
					'type' => 'image',
					'instructions' => 'Home page hero image dimensions are 2000x600.',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'return_format' => 'id',
					'preview_size' => 'sa-hero-front-page',
					'library' => 'all',
					'min_width' => 2000,
					'min_height' => '',
					'min_size' => '',
					'max_width' => '',
					'max_height' => 600,
					'max_size' => '',
					'mime_types' => '.jpg,.png,.svg',
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'options_page',
						'operator' => '==',
						'value' => 'student-affairs-child',
					),
				),
			),
			'menu_order' => 0,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => '',
			'active' => 1,
			'description' => '',
		));

		// Add field group for hero image attachment ID for hero image post types
		acf_add_local_field_group(array (
			'key' => 'group_565dbe34e8c25',
			'title' => 'Hero Image',
			'fields' => array (
				array (
					'key' => 'field_565dbe5daf34e',
					'label' => 'Hero Image',
					'name' => '_sa_hero_image_attachment_id',
					'type' => 'image',
					'instructions' => 'The "hero image" is the big banner image at the top of the page.',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'return_format' => 'id',
					'preview_size' => 'sa-hero-inside-page',
					'library' => 'all',
					'min_width' => 2000,
					'min_height' => 300,
					'min_size' => '',
					'max_width' => 2000,
					'max_height' => 300,
					'max_size' => '',
					'mime_types' => '',
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'hero_images',
					),
				),
			),
			'menu_order' => 0,
			'position' => 'acf_after_title',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => '',
			'active' => 1,
			'description' => '',
		));

	endif;

}, 15 );