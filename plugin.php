<?php

/**
 * Plugin Name:       Student Affairs - Images
 * Plugin URI:        https://sa.ua.edu
 * Description:       This WordPress plugin is intended solely for The University of Alabama Division of Student Affairs.
 * Version:           1.0
 * Author:            UA Division of Student Affairs - Rachel Carden
 * Author URI:        https://sa.ua.edu
 */

// We only need you in the admin
if ( is_admin() ) {
	require_once plugin_dir_path( __FILE__ ) . 'includes/admin.php';
}

// Runs on install
register_activation_hook( __FILE__, 'ua_sa_images_install' );
function ua_sa_images_install() {

	// Register the custom post types
	ua_sa_images_register_cpts();

	// Flush the rewrite rules to start fresh
	flush_rewrite_rules();

}

// Runs when the plugin is upgraded
// @TODO test to see if this only runs when running a bulk upgrade
add_action( 'upgrader_process_complete', 'ua_sa_images_upgrader_process_complete', 1, 2 );
function ua_sa_images_upgrader_process_complete( $upgrader, $upgrade_info ) {

	// Flush the rewrite rules to start fresh
	flush_rewrite_rules();

}

// Setup styles and scripts
/*add_action( 'wp_enqueue_scripts', function () {
	global $post;

	// Get the plugin path
	$plugin_dir = plugin_dir_url( __FILE__ );

	// Enqueue styles
	wp_enqueue_style( 'sa-images', $plugin_dir . 'css/sa-images.min.css', array() );

	// Enqueue complaints script
	if ( isset( $post->post_content ) && has_shortcode( $post->post_content, 'print_sa_complaints_appeals' ) ) {
		wp_register_script( 'foundation', $plugin_dir . 'js/foundation.min.js', array( 'jquery' ) );
		wp_register_script( 'foundation-tab', $plugin_dir . 'js/foundation.tab.min.js', array( 'foundation' ) );
		wp_enqueue_script( 'sa-complaints', $plugin_dir . 'js/sa-complaints.min.js', array( 'jquery', 'foundation-tab' ) );
	}

}, 30 );*/

// Register any REST fields
add_action( 'rest_api_init', function() {

	// Register 'hero_image' field
	register_rest_field( 'hero_images', 'hero_image', array(
		'get_callback'    => 'ua_sa_images_get_field_value',
		'update_callback' => null,
		'schema'          => null,
	));

});

// Get REST field value
function ua_sa_images_get_field_value( $object, $field_name, $request ) {
	if ( 'hero_images' == $object[ 'type' ] ) {

		switch( $field_name ) {

			// Get hero image
			case 'hero_image':
				if ( ( $hero_image_id = get_post_meta( $object[ 'id' ], '_sa_hero_image_attachment_id', true ) )
				     && ( $hero_image_src = wp_get_attachment_image_src( $hero_image_id, 'sa-hero-inside-page' ) ) ) {
					return $hero_image_src[0];
				}
		}

	}
	return '';
}

// Register custom post types
add_action( 'init', 'ua_sa_images_register_cpts', 1 );
function ua_sa_images_register_cpts() {
	global $blog_id;

	// Only register on main site
	if ( ! $blog_id || ! is_main_site( $blog_id ) ) {
		return false;
	}

	// Register hero images CPT
	register_post_type( 'hero_images', array(
		'labels' => array(
			'name'               => _x( 'Hero Images', 'post type general name', 'student-affairs-framework' ),
			'singular_name'      => _x( 'Hero Image', 'post type singular name', 'student-affairs-framework' ),
			'menu_name'          => _x( 'Hero Images', 'admin menu', 'student-affairs-framework' ),
			'name_admin_bar'     => _x( 'Hero Images', 'add new on admin bar', 'student-affairs-framework' ),
			'add_new'            => _x( 'Add New', 'hero_images', 'student-affairs-framework' ),
			'add_new_item'       => __( 'Add New Hero Image', 'student-affairs-framework' ),
			'new_item'           => __( 'New Hero Image', 'student-affairs-framework' ),
			'edit_item'          => __( 'Edit Hero Image', 'student-affairs-framework' ),
			'view_item'          => __( 'View Hero Image', 'student-affairs-framework' ),
			'all_items'          => __( 'All Hero Images', 'student-affairs-framework' ),
			'search_items'       => __( 'Search Hero Images', 'student-affairs-framework' ),
			'parent_item_colon'  => __( 'Parent Hero Image:', 'student-affairs-framework' ),
			'not_found'          => __( 'No hero images found.', 'student-affairs-framework' ),
			'not_found_in_trash' => __( 'No hero images found in Trash.', 'student-affairs-framework' )
		),
		'public'                => true,
		'publicly_queryable'    => true,
		'exclude_from_search'   => true,
		'show_in_nav_menus'     => false,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'show_in_admin_bar'     => true,
		'menu_icon'             => 'dashicons-format-image',
		'capabilities'          => array(
			'edit_post'         => 'edit_hero_image',
			'read_post'         => 'read_hero_image',
			'delete_post'       => 'delete_hero_image',
			'edit_posts'        => 'edit_hero_images',
			'edit_others_posts' => 'edit_others_hero_images',
			'publish_posts'     => 'publish_hero_images',
			'read_private_posts'=> 'read_private_hero_images',
			'read'              => 'read',
			'delete_posts'      => 'delete_hero_images',
			'delete_private_posts' => 'delete_private_hero_images',
			'delete_published_posts' => 'delete_published_hero_images',
			'delete_others_posts' => 'delete_others_hero_images',
			'edit_private_posts' => 'edit_private_hero_images',
			'edit_published_posts' => 'edit_published_hero_images',
			'create_posts'      => 'edit_hero_images'
		),
		'hierarchical'          => false,
		'supports'              => array( 'title' ),
		'has_archive'           => false,
		'rewrite'               => false,
		'query_var'             => true,
		'show_in_rest'          => true,
	) );

}